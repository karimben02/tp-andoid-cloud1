package fr.anthony.minitp1.tp4.animals.dao


import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import fr.anthony.minitp1.tp4.animals.model.AnimalsRoom


@Dao
interface AnimalsDao {


    @Query("SELECT * FROM animal_test")
    fun selectAll() : LiveData<List<AnimalsRoom>>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(animalsRoom: AnimalsRoom)


    @Query("DELETE FROM animal_test")
    fun deleteAll()
}

