package fr.anthony.minitp1.tp4.animals.remote

import fr.anthony.minitp1.tp4.animals.model.AnimalsRetrofit
import retrofit2.http.GET

interface AnimalsEndPoint {

    @GET("/cat?json=true")
    suspend fun getRandom() : AnimalsRetrofit
}