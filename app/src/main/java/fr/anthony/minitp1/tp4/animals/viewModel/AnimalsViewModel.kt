package fr.anthony.minitp1.tp4.animals.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import fr.anthony.minitp1.tp4.animals.model.AnimalsRoom
import fr.anthony.minitp1.tp4.animals.model.AnimalsUi
import fr.anthony.minitp1.tp4.animals.repository.AnimalQuoteRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class AnimalsViewModel : ViewModel() {

    private val mAnimalsRepository: AnimalQuoteRepository by lazy { AnimalQuoteRepository() }
    var mAnimalsLiveData: LiveData<List<AnimalsUi>> =
        mAnimalsRepository.selectAllAnimals().map {
            it.toUi()
        }


    fun fetchNewQuote() {
        viewModelScope.launch(Dispatchers.IO) {
            mAnimalsRepository.fetchData()
        }
    }

    fun deleteAll() {
        viewModelScope.launch(Dispatchers.IO) {
            mAnimalsRepository.deleteAllAnimals()
        }
    }
}


private fun List<AnimalsRoom>.toUi(): List<AnimalsUi> {
    return asSequence().map {
        AnimalsUi(
            date1 = it.date1,
            iconUrl = it.iconUrl
        )
    }.toList()
}
