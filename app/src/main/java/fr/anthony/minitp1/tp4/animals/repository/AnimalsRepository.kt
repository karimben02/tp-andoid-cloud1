package fr.anthony.minitp1.tp4.animals.repository

import androidx.lifecycle.LiveData
import fr.anthony.minitp1.architecture.CustomApplication
import fr.anthony.minitp1.architecture.RetrofitBuilder
import fr.anthony.minitp1.tp4.animals.model.AnimalsRetrofit
import fr.anthony.minitp1.tp4.animals.model.AnimalsRoom
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AnimalQuoteRepository {
    private val mAnimalsDao = CustomApplication.instance.mApplicationDatabase.animalsDao()

    fun selectAllAnimals(): LiveData<List<AnimalsRoom>> {
        return mAnimalsDao.selectAll()
    }


    private suspend fun insertAnimals(animalQuote: AnimalsRoom) =
        withContext(Dispatchers.IO) {
            mAnimalsDao.insert(animalQuote)
        }


    suspend fun deleteAllAnimals() = withContext(Dispatchers.IO) {
        mAnimalsDao.deleteAll()
    }


    suspend fun fetchData() {
        insertAnimals(RetrofitBuilder.getAnimals().getRandom().toRoom())
    }
}


private fun AnimalsRetrofit.toRoom(): AnimalsRoom {
    return AnimalsRoom(
        date1 = "crée le : " +date1,
        iconUrl = "https://cataas.com/"+iconUrl
    )
}




