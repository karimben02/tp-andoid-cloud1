package fr.anthony.minitp1.tp4.animals.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.anthony.minitp1.databinding.ActivityAnimalsBinding
import fr.anthony.minitp1.tp4.animals.model.AnimalsUi
import fr.anthony.minitp1.tp4.animals.viewModel.AnimalsAdapter
import fr.anthony.minitp1.tp4.animals.viewModel.AnimalsViewModel

class AnimalsActivity : AppCompatActivity() {

    private lateinit var viewModel: AnimalsViewModel
    private lateinit var binding: ActivityAnimalsBinding
    private val adapter: AnimalsAdapter = AnimalsAdapter()
    private val observer = Observer<List<AnimalsUi>> {
        adapter.submitList(it)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAnimalsBinding.inflate(layoutInflater)
        setContentView(binding.root)


        viewModel = ViewModelProvider(this)[AnimalsViewModel::class.java]


        binding.animalsActivityRv.layoutManager =
            LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        binding.animalsActivityRv.adapter = adapter


        binding.animalsActivityAdd.setOnClickListener {
            viewModel.fetchNewQuote()
        }


        binding.animalsActivityDelete.setOnClickListener {
            viewModel.deleteAll()
        }
    }

    override fun onStart() {
        super.onStart()
        viewModel.mAnimalsLiveData.observe(this, observer)
    }


    override fun onStop() {
        viewModel.mAnimalsLiveData.removeObserver(observer)
        super.onStop()
    }
}