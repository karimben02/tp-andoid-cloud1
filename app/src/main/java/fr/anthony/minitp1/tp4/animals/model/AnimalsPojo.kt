package fr.anthony.minitp1.tp4.animals.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/** Object use for room */
@Entity(tableName = "animal_test")
data class AnimalsRoom(
    @ColumnInfo(name = "test_text")
    val date1: String,


    @ColumnInfo(name = "test_icon_url")
    val iconUrl: String
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}


/** Object use for Ui */
data class AnimalsUi(
    val date1: String,
    val iconUrl: String
)

data class AnimalsRetrofit(

    @Expose
    @SerializedName("created_at")
    val date1: String,

    @Expose
    @SerializedName("url")
    val iconUrl: String
)
