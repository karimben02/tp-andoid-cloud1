package fr.anthony.minitp1.tp4.animals.viewModel

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fr.anthony.minitp1.databinding.ItemAnimalsBinding
import fr.anthony.minitp1.tp4.animals.model.AnimalsUi


val diffUtils = object : DiffUtil.ItemCallback<AnimalsUi>() {
    override fun areItemsTheSame(oldItem: AnimalsUi, newItem: AnimalsUi): Boolean {
        return oldItem == newItem
    }


    override fun areContentsTheSame(oldItem: AnimalsUi, newItem: AnimalsUi): Boolean {
        return oldItem == newItem
    }
}

class AnimalsQuoteViewHolder(
    val binding: ItemAnimalsBinding
) : RecyclerView.ViewHolder(binding.root) {

    private lateinit var ui: AnimalsUi

    fun bind(AnimalsUi: AnimalsUi) {
        ui = AnimalsUi
        Glide.with(itemView.context)
            .load(AnimalsUi.iconUrl)
            .into(binding.itemAnimalsIcon)


        binding.itemAnimalsQuote.text = AnimalsUi.date1
    }
}

class AnimalsAdapter : ListAdapter<AnimalsUi, AnimalsQuoteViewHolder>(diffUtils) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnimalsQuoteViewHolder {
        return AnimalsQuoteViewHolder(
            ItemAnimalsBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }


    override fun onBindViewHolder(holder: AnimalsQuoteViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

