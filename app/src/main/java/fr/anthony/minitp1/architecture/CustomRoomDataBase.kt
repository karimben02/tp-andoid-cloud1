package fr.anthony.minitp1.architecture

import androidx.room.Database
import androidx.room.RoomDatabase
import fr.anthony.minitp1.oldfeatures2.dao.AndroidVersionDao2
import fr.anthony.minitp1.oldfeatures2.model.LocalObjectDataSample
import fr.anthony.minitp1.tp4.animals.dao.AnimalsDao
import fr.anthony.minitp1.tp4.animals.model.AnimalsRoom


@Database(
    entities = [
        LocalObjectDataSample::class,
        AnimalsRoom::class
    ],
    version = 2,
    exportSchema = false
)
abstract class CustomRoomDatabase : RoomDatabase() {


    abstract fun androidVersionDao(): AndroidVersionDao2


    abstract fun animalsDao() : AnimalsDao
}


