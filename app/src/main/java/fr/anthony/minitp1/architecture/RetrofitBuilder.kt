package fr.anthony.minitp1.architecture

import com.google.gson.GsonBuilder
import fr.anthony.minitp1.tp4.animals.remote.AnimalsEndPoint
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitBuilder {


    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl("https://cataas.com/")
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()))
        .build()

    fun getAnimals(): AnimalsEndPoint = retrofit.create(AnimalsEndPoint::class.java)
}
