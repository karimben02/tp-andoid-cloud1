package fr.anthony.minitp1.oldfeatures2


import android.os.Bundle
import android.view.HapticFeedbackConstants
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.anthony.minitp1.databinding.ActivityRecyclerVoitureBinding
import fr.anthony.minitp1.oldfeatures2.model.MyObjectForRecyclerView
import fr.anthony.minitp1.oldfeatures2.model.ObjectDataSample
import fr.anthony.minitp1.oldfeatures2.viewmodel.AndroidVersionViewModel
import kotlin.random.Random


class RecyclerViewActivity : AppCompatActivity() {


    private lateinit var adapter: AdapterVoiture
    private lateinit var binding: ActivityRecyclerVoitureBinding
    private lateinit var viewModel: AndroidVersionViewModel


    private val androidVersionListObserver = Observer<List<MyObjectForRecyclerView>> {
        adapter.submitList(it)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRecyclerVoitureBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.buttonAddItem.setOnClickListener { addRandomAndroidVersion() }
        binding.deleteAllItemButton.setOnClickListener { deleteAndroidVersion() }

        viewModel = ViewModelProvider(this)[AndroidVersionViewModel::class.java]
        adapter = AdapterVoiture { item, view ->
            onItemClick(item, view)
        }

        // We define the style
        binding.recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)


        // We set the adapter to recycler view
        binding.recyclerView.adapter = adapter
    }


    override fun onStart() {
        super.onStart()
        viewModel.androidVersionList.observe(this, androidVersionListObserver)
    }


    override fun onStop() {
        super.onStop()
        viewModel.androidVersionList.observe(this, androidVersionListObserver)
    }

    private fun addRandomAndroidVersion() {
        val random = Random.nextInt(0,1000)
        viewModel.insertAndroidVersion("Android $random", random, "url:$random")
    }


    private fun deleteAndroidVersion() {
        viewModel.deleteAllAndroidVersion()
    }

    private fun onItemClick(objectDataSample: ObjectDataSample, view: View) {
        view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
        Toast.makeText(this, objectDataSample.versionName, Toast.LENGTH_LONG).show()
        addRandomAndroidVersion()
    }


}


