package fr.anthony.minitp1.oldfeatures2.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.anthony.minitp1.oldfeatures2.model.MyObjectForRecyclerView
import fr.anthony.minitp1.oldfeatures2.model.ObjectDataSample
import fr.anthony.minitp1.oldfeatures2.repository.AndroidVersionRepository

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class AndroidVersionViewModel : ViewModel() {


    private val _androidVersionList = MutableLiveData<List<MyObjectForRecyclerView>>()
    private val androidVersionRepository: AndroidVersionRepository by lazy { AndroidVersionRepository() }

    val androidVersionList: LiveData<List<MyObjectForRecyclerView>> get() = _androidVersionList


    init {

    }

    fun insertAndroidVersion(androidName: String, androidCode: Int, url: String) {
        viewModelScope.launch(Dispatchers.IO) {
            androidVersionRepository.insertAndroidVersion(
                ObjectDataSample(androidName, androidCode, url)
            )
        }
    }


    fun deleteAllAndroidVersion() {
        viewModelScope.launch(Dispatchers.IO) {
            androidVersionRepository.deleteAllAndroidVersion()
        }
    }

}