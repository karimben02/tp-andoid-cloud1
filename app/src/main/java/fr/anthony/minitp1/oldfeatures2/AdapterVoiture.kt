package fr.anthony.minitp1.oldfeatures2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import fr.anthony.minitp1.databinding.ItemCustomRecyclerBinding
import com.bumptech.glide.Glide
import fr.anthony.minitp1.R
import fr.anthony.minitp1.oldfeatures2.model.MyObjectForRecyclerView
import fr.anthony.minitp1.oldfeatures2.model.ObjectDataHeaderSample
import fr.anthony.minitp1.oldfeatures2.model.ObjectDataSample


private val diffItemUtils = object : DiffUtil.ItemCallback<MyObjectForRecyclerView>() {


    override fun areItemsTheSame(
        oldItem: MyObjectForRecyclerView,
        newItem: MyObjectForRecyclerView
    ): Boolean {
        return oldItem == newItem
    }


    override fun areContentsTheSame(
        oldItem: MyObjectForRecyclerView,
        newItem: MyObjectForRecyclerView
    ): Boolean {
        return oldItem == newItem
    }
}


class AdapterVoiture(
    private val onItemClick: (quoteUi: ObjectDataSample, view: View) -> Unit,
) : ListAdapter<MyObjectForRecyclerView, RecyclerView.ViewHolder>(diffItemUtils) {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        when (viewType) {
            MyItemType.ROW.type -> {
                AndroidVersionViewHolder(
                    ItemCustomRecyclerBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    ),onItemClick
                )
            }
            else -> throw RuntimeException("Wrong view type received $viewType")
        }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) =
        when (holder.itemViewType) {
            MyItemType.ROW.type -> (holder as AndroidVersionViewHolder).bind(getItem(position) as ObjectDataSample)

            else -> throw RuntimeException("Wrong view type received ${holder.itemView}")
        }


    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is ObjectDataSample -> MyItemType.ROW.type
            is ObjectDataHeaderSample -> MyItemType.HEADER.type
        }
    }
}


//class AndroidVersionAdapter {
//    //TODO
//}


class AndroidVersionViewHolder(
    private val binding: ItemCustomRecyclerBinding,
    onItemClick: (quoteUi: ObjectDataSample, view: View) -> Unit
) : RecyclerView.ViewHolder(binding.root) {
    private lateinit var ui: ObjectDataSample
    init {
        binding.root.setOnClickListener {
            onItemClick(ui, itemView)
        }
    }
    fun bind(objectDataSample: ObjectDataSample) {
        ui = objectDataSample
        binding.itemRecyclerViewVersionName.text = objectDataSample.versionName
        binding.itemRecyclerViewVersionCode.text = "${objectDataSample.versionCode}"
        Glide.with(itemView.context)
            .load(objectDataSample.versionImage)
            .placeholder(R.drawable.ic_launcher_background)
            .into(binding.itemRecyclerViewVersionImage)
    }

}

class AndroidVersionHeaderViewHolder(

    onItemClick: (quoteUi: ObjectDataSample, view: View) -> Unit
)

enum class MyItemType(val type: Int) {
    ROW(0),
    HEADER(1)
}

