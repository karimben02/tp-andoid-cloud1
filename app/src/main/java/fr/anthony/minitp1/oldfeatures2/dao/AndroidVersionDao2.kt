package fr.anthony.minitp1.oldfeatures2.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import fr.anthony.minitp1.oldfeatures2.model.LocalObjectDataSample

@Dao
interface AndroidVersionDao2 {


    @Query("SELECT * FROM android_version_object_table ORDER BY name ASC")
    fun selectAll(): LiveData<List<LocalObjectDataSample>>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(androidVersion: LocalObjectDataSample)


    @Query("DELETE FROM android_version_object_table")
    fun deleteAll()
}
