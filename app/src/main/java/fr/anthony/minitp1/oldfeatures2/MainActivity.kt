package fr.anthony.minitp1.oldfeatures2

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import fr.anthony.minitp1.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

}