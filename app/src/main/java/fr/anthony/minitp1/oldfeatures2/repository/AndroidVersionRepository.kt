package fr.anthony.minitp1.oldfeatures2.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import fr.anthony.minitp1.architecture.CustomApplication
import fr.anthony.minitp1.oldfeatures2.model.LocalObjectDataSample
import fr.anthony.minitp1.oldfeatures2.model.MyObjectForRecyclerView
import fr.anthony.minitp1.oldfeatures2.model.ObjectDataHeaderSample
import fr.anthony.minitp1.oldfeatures2.model.ObjectDataSample


class AndroidVersionRepository {

    /*fun generateFakeData(): MutableList<MyObjectForRecyclerView> {
        val result = mutableListOf<MyObjectForRecyclerView>()
        // Create data raw
        mutableListOf(
            ObjectDataSample("Android Lollipop", 5, "https://picsum.photos/seed/picsum/200/300"),
            ObjectDataSample("Android Marshmallow", 6, "https://picsum.photos/seed/picsum/200/300"),
            ObjectDataSample("Android Nougat", 7, "https://picsum.photos/seed/picsum/200/300"),
            ObjectDataSample("Android Oreo", 8, "https://picsum.photos/seed/picsum/200/300"),
            ObjectDataSample("Android Pie", 9, "https://picsum.photos/seed/picsum/200/300"),
            ObjectDataSample("Android Q", 10, "https://picsum.photos/seed/picsum/200/300"),
            ObjectDataSample("Android R", 11, "https://picsum.photos/seed/picsum/200/300"),
            ObjectDataSample("Android S", 12, "https://picsum.photos/seed/picsum/200/300")
        ).groupBy {
            // Split in 2 list, modulo and not
            it.versionCode % 2 == 0
        }.forEach { (isModulo, items) ->
            // For each mean for each list split
            // Here we have a map (key = isModulo) and each key have a list of it's items
            result.add(ObjectDataHeaderSample("Is modulo : $isModulo"))
            result.addAll(items)
        }
        return result


    }*/


    private val mandroidVersionDao =
        CustomApplication.instance.mApplicationDatabase.androidVersionDao();


    fun selectAllAndroidVersion(): LiveData<List<ObjectDataSample>> {
        return mandroidVersionDao.selectAll().map { list ->
            list.toObjectDataSample()
        }
    }


    fun insertAndroidVersion(objectDataSample: ObjectDataSample) {
        mandroidVersionDao.insert(objectDataSample.toRoomObject())
    }


    fun deleteAllAndroidVersion() {
        mandroidVersionDao.deleteAll()
    }


}
private fun ObjectDataSample.toRoomObject(): LocalObjectDataSample {
    return LocalObjectDataSample(
        name = versionName,
        code = versionCode,
        image = versionImage
    )
}


private fun List<LocalObjectDataSample>.toObjectDataSample(): List<ObjectDataSample> {
    return map { eachItem ->
        ObjectDataSample(
            versionCode = eachItem.code,
            versionName = eachItem.name,
            versionImage = eachItem.image
        )
    }
}

private fun List<ObjectDataSample>.toMyObjectForRecyclerView(): List<MyObjectForRecyclerView> {
    val result = mutableListOf<MyObjectForRecyclerView>()


    groupBy {
        // Split in 2 list, modulo and not
        it.versionCode % 2 == 0
    }.forEach { (isModulo, items) ->
        // For each mean for each list split
        // Here we have a map (key = isModulo) and each key have a list of it's items
        result.add(ObjectDataHeaderSample("Is modulo : $isModulo"))
        result.addAll(items)
    }
    return result
}